package com.example.edtechhackathon.internshipsRequestRecyclerView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.edtechhackathon.R;

import java.util.ArrayList;

public class RecyclerViewAdapterRequests extends RecyclerView.Adapter<RecyclerViewAdapterRequests.RecyclerViewViewHolder> {

    private ArrayList<RecyclerViewItemRequest> arrayList;
    protected Context context;

    @NonNull
    @Override
    public RecyclerViewViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.request_card,parent,false);
        RecyclerViewViewHolder recyclerViewViewHolder = new RecyclerViewViewHolder(view);
        return recyclerViewViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewViewHolder holder, int position) {
        RecyclerViewItemRequest recyclerViewItemRequest = arrayList.get(position);

        holder.textViewName.setText(recyclerViewItemRequest.getName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, Integer.toString(position), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public RecyclerViewAdapterRequests(ArrayList<RecyclerViewItemRequest> arrayList, Context context){
        this.arrayList = arrayList;
        this.context = context;
    }

    public class RecyclerViewViewHolder extends RecyclerView.ViewHolder{

        public TextView textViewName;


        public RecyclerViewViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewName=itemView.findViewById(R.id.name);
        }
    }

}
