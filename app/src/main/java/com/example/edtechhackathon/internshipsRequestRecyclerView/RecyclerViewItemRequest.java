package com.example.edtechhackathon.internshipsRequestRecyclerView;

public class RecyclerViewItemRequest {

    private String UID;

    public RecyclerViewItemRequest(String UID) {
        this.UID=UID;
    }

    public RecyclerViewItemRequest(){

    }

    public String getName() {
        return UID;
    }

    public void setName(String UID) {
        this.UID = UID;
    }
}
