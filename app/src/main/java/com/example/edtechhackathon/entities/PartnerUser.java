package com.example.edtechhackathon.entities;

import android.graphics.Bitmap;

import java.util.ArrayList;

public class PartnerUser implements IPartnerUser {

    private String email = "";
    private String name = "";
    private String about = "";
    private String role = "";
    private String siteURL = "";
    private String headquarter = "";

    private ArrayList<?> feedbacksHistory = null;
    private ArrayList<?> internshipsList = null;
    private ArrayList<?> notifications = null;

    private Bitmap profileICO = null;

    public PartnerUser(String email, String name, String about, String role, String siteURL, String headquarter) {
        this.email = email;
        this.name = name;
        this.about = about;
        this.role = role;
        this.siteURL = siteURL;
        this.headquarter = headquarter;
        this.feedbacksHistory = new ArrayList<>();
        this.internshipsList = new ArrayList<>();
        this.notifications = new ArrayList<>();
    }

    public PartnerUser(){

    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getSiteURL() {
        return siteURL;
    }

    public void setSiteURL(String siteURL) {
        this.siteURL = siteURL;
    }

    public String getHeadquarter() {
        return headquarter;
    }

    public void setHeadquarter(String headquarter) {
        this.headquarter = headquarter;
    }

    public ArrayList<?> getFeedbacksHistory() {
        return feedbacksHistory;
    }

    public void setFeedbacksHistory(ArrayList<?> feedbacksHistory) {
        this.feedbacksHistory = feedbacksHistory;
    }

    public ArrayList<?> getInternshipsList() {
        return internshipsList;
    }

    public void setInternshipsList(ArrayList<?> internshipsList) {
        this.internshipsList = internshipsList;
    }

    public ArrayList<?> getNotifications() {
        return notifications;
    }

    public void setNotifications(ArrayList<?> notifications) {
        this.notifications = notifications;
    }

    public Bitmap getProfileICO() {
        return profileICO;
    }

    public void setProfileICO(Bitmap profileICO) {
        this.profileICO = profileICO;
    }

    @Override
    public void createInternship() {

    }

    @Override
    public void deleteInternship() {

    }

    @Override
    public void acceptStudentInternshipRequest() {

    }

    @Override
    public void cancelStudentInternshipRequest() {

    }
}
