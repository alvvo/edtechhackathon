package com.example.edtechhackathon.entities;

public interface IPartnerUser {

    public void createInternship();

    public void deleteInternship();

    public void acceptStudentInternshipRequest();

    public void cancelStudentInternshipRequest();

}
