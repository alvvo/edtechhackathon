package com.example.edtechhackathon.entities;

public class StudentUser implements IStudentUser {

    private String email;
    private String name;
    private String surname;
    private String secondName;
    private String linkedInURL;
    private String date;
    private String role;
    private String city;
    private String university;
    private String specialization;

    private double rating = 0.0;


    public StudentUser(String email, String name, String surname, String secondName, String linkedInURL, String date, String role, String city, String university, String specialization, double rating) {
        this.email = email;
        this.name = name;
        this.surname = surname;
        this.secondName = secondName;
        this.linkedInURL = linkedInURL;
        this.date = date;
        this.role = role;
        this.city = city;
        this.university = university;
        this.specialization = specialization;
        this.rating = rating;
    }

    public StudentUser(){
        email = "";
        name = "";
        surname = "";
        secondName = "";
        linkedInURL = "";
        date = "";
        role = "";
        city = "";
        university = "";
        specialization = "";
        role = "";
        rating = 0.0;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getLinkedInURL() {
        return linkedInURL;
    }

    public void setLinkedInURL(String linkedInURL) {
        this.linkedInURL = linkedInURL;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getUniversity() {
        return university;
    }

    public void setUniversity(String university) {
        this.university = university;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    @Override
    public void respondToInternship() {

    }

    @Override
    public void cancelRespondToInternship() {

    }

    @Override
    public void createCV() {
    }

    @Override
    public String getCVId() {
        return null;
    }

    @Override
    public void createInternshipFeedback() {

    }
}
