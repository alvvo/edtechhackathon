package com.example.edtechhackathon.entities;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.ExecutionException;

public class Coords {

    private double latitude = 0.0;
    private double longitude = 0.0;
    private String id = "";
    final private String apiKey = "238cdfab-51ee-4ab1-80f2-5dc0bffacc37";
    private String URL_TEMPLATE = "https://graphhopper.com/api/1/geocode?q=%s&locale=ru&debug=true&key=%s";

    public Coords() {
        this.latitude = 0.0;
        this.longitude = 0.0;
    }

    public Coords(double latitude, double longitude, String id) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.id = id;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public void createCoords(String address){
        DownloadCooords task = new DownloadCooords();
        String url = String.format(URL_TEMPLATE, address, apiKey);
        try {
            String result =  task.execute(url).get();
            Log.i("Coords", result);
            JSONObject jsonObject = new JSONObject(result);
            JSONArray hits = jsonObject.getJSONArray("hits");
            JSONObject hitsObj = hits.getJSONObject(0);
            JSONObject point = hitsObj.getJSONObject("point");
            latitude = Double.parseDouble(point.getString("lat"));
            longitude = Double.parseDouble(point.getString("lng"));
            Log.i("!@#", String.valueOf(latitude));
            Log.i("!@#", String.valueOf(longitude));

        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private static class DownloadCooords extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... strings) {
            URL url = null;
            HttpURLConnection urlConnection = null;
            StringBuilder result = new StringBuilder();
            try {
                url = new URL(strings[0]);
                urlConnection = (HttpURLConnection) url.openConnection();
                InputStream inputStream = urlConnection.getInputStream();
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String line = bufferedReader.readLine();
                while (line != null){
                    result.append(line);
                    line = bufferedReader.readLine();
                }
                return result.toString();

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (urlConnection != null){
                    urlConnection.disconnect();
                }
            }

            return null;
        }
    }

}