package com.example.edtechhackathon.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import com.example.edtechhackathon.DBHelper;
import com.example.edtechhackathon.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ProfileStudentFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProfileStudentFragment extends Fragment {

    DBHelper dbHelper = new DBHelper();

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    //Components
    TextView userName;
    TextView userRole;
    TextView userCity;
    TextView userEmail;
    TextView userUniversity;
    TextView userSpecialization;
    TextView userCity2;

    TextView skillName;

    CardView cardView;

    LinearLayout skillsLayout;

    ArrayList<String> skills = new ArrayList<>();

    public ProfileStudentFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ProfileStudentFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ProfileStudentFragment newInstance(String param1, String param2) {
        ProfileStudentFragment fragment = new ProfileStudentFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        userName = getView().findViewById(R.id.userName);
        userRole = getView().findViewById(R.id.userRole);
        userCity = getView().findViewById(R.id.userCity);
        userEmail = getView().findViewById(R.id.userEmail);
        userUniversity = getView().findViewById(R.id.userUniversity);
        userSpecialization = getView().findViewById(R.id.textViewSpecialization);
        userCity2 = getView().findViewById(R.id.textViewCity2);
        skillsLayout=getView().findViewById(R.id.skillsLayout);
        skillName=getView().findViewById(R.id.skillName);
        cardView=getView().findViewById(R.id.skillCard);
        fillTheGaps();
        readSkills();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile_student, container, false);
    }

    private void readSkills(){
        dbHelper.getDatabaseReference().addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                skillsLayout.removeAllViews();
                for(DataSnapshot dataSnapshot : snapshot.child("USER_DATA").child("STUDENTS").child(dbHelper.getmAuth().getUid()).child("Skills").getChildren()){
                    String str = dataSnapshot.getValue(String.class);
                    TextView textView = new TextView(getContext());
                    textView.setText(str);
                    textView.setTextColor(Color.WHITE);
                    textView.setBackground(getResources().getDrawable(R.drawable.red_rating_bg));
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    params.setMargins(32,10,32,10);
                    textView.setLayoutParams(params);
                    params.gravity = Gravity.CENTER;
                    skillsLayout.addView(textView);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void fillTheGaps(){
        if (dbHelper.getmAuth().getUid().contains("CFKrx8eyIMPzZWVAo2PdKVt31Y13")){
            LinearLayout ratingLayout = getView().findViewById(R.id.ratings);
            HorizontalScrollView skills = getView().findViewById(R.id.skills);
            TextView textViewSkillHeader = getView().findViewById(R.id.skillHeader);
            ImageButton editSkills = getView().findViewById(R.id.editSkills);
            TextView docHeader = getView().findViewById(R.id.DocHeader);
            CardView cardViewDocument = getView().findViewById(R.id.doc_card);
            ratingLayout.setVisibility(View.GONE);
            skills.setVisibility(View.GONE);
            textViewSkillHeader.setVisibility(View.GONE);
            editSkills.setVisibility(View.GONE);
            cardViewDocument.setVisibility(View.GONE);
            docHeader.setVisibility(View.GONE);
            /*

            Тут вставить разметку и установить ей setVisibility(View.Visible);

             */
            dbHelper.getDatabaseReference().addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    String name = snapshot.child("USER_DATA").child("PARTNERS").child(dbHelper.getmAuth().getUid()).child("Name").getValue(String.class);
                    userName.setText(name);
                    String role = snapshot.child("USER_DATA").child("PARTNERS").child(dbHelper.getmAuth().getUid()).child("Role").getValue(String.class);
                    userRole.setText(role);
                    String city = snapshot.child("USER_DATA").child("PARTNERS").child(dbHelper.getmAuth().getUid()).child("Headquarter").getValue(String.class);
                    userCity.setText(city);
                    String email = snapshot.child("USER_DATA").child("PARTNERS").child(dbHelper.getmAuth().getUid()).child("Email").getValue(String.class);
                    userEmail.setText(email);
                    userUniversity.setText(email);
                    String site = snapshot.child("USER_DATA").child("PARTNERS").child(dbHelper.getmAuth().getUid()).child("SiteURL").getValue(String.class);
                    userSpecialization.setText(site);
                    userCity2.setText(city);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
        } else {
        dbHelper.getDatabaseReference().addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                String name = snapshot.child("USER_DATA").child("STUDENTS").child(dbHelper.getmAuth().getUid()).child("Name").getValue(String.class);
                userName.setText(name);

                String city = snapshot.child("USER_DATA").child("STUDENTS").child(dbHelper.getmAuth().getUid()).child("city").getValue(String.class);
                userCity.setText(city);
                userCity2.setText(city);

                String email = snapshot.child("USER_DATA").child("STUDENTS").child(dbHelper.getmAuth().getUid()).child("Email").getValue(String.class);
                userEmail.setText(email);

                String university = snapshot.child("USER_DATA").child("STUDENTS").child(dbHelper.getmAuth().getUid()).child("university").getValue(String.class);
                userUniversity.setText(university);

                String specialization = snapshot.child("USER_DATA").child("STUDENTS").child(dbHelper.getmAuth().getUid()).child("specialization").getValue(String.class);
                userSpecialization.setText(specialization);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
        }
    }

    public void applySkillToDatabase(){

    }
}