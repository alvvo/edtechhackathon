package com.example.edtechhackathon.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.edtechhackathon.DBHelper;
import com.example.edtechhackathon.R;
import com.example.edtechhackathon.dashboardRecyclerView.RecyclerViewAdapter;
import com.example.edtechhackathon.dashboardRecyclerView.RecyclerViewItem;
import com.example.edtechhackathon.eventRecyclerView.RecyclerViewAdapterEvents;
import com.example.edtechhackathon.eventRecyclerView.RecyclerViewItemEvent;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link EventsStudentFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EventsStudentFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public EventsStudentFragment() {
        // Required empty public constructor
    }

    ArrayList<RecyclerViewItemEvent> recyclerViewItemEvents = new ArrayList<RecyclerViewItemEvent>();
    DBHelper dbHelper = new DBHelper();


    RecyclerView recyclerViewEvent=null;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;



    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment EventsStudentFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static EventsStudentFragment newInstance(String param1, String param2) {
        EventsStudentFragment fragment = new EventsStudentFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerViewEvent = getView().findViewById(R.id.recyclerEvent);
        adapter = new RecyclerViewAdapterEvents(recyclerViewItemEvents, getContext());
        layoutManager = new LinearLayoutManager(getContext());
        recyclerViewEvent.setAdapter(adapter);
        recyclerViewEvent.setLayoutManager(layoutManager);

        dbHelper.getDatabaseReference().child("USER_DATA").child("EVENTS").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for(DataSnapshot data : snapshot.getChildren()){
                    Log.i("HUY",data.toString());
                    RecyclerViewItemEvent recyclerViewItem = data.getValue(RecyclerViewItemEvent.class);
                    recyclerViewItemEvents.add(recyclerViewItem);
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_events_student, container, false);
    }
}