package com.example.edtechhackathon;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.example.edtechhackathon.activities.CreateInternshipActivity;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class FinalSearchActivity extends AppCompatActivity {
    private LayoutInflater layoutInflater;
    private LinearLayout linearLayout;
    DBHelper dbHelper = new DBHelper();
    String rule;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_final_search);
        Intent intent = getIntent();
        rule = intent.getStringExtra("rule");
        init();
    }

    private void addToLinearLayout(CardView cardView){
        linearLayout.addView(cardView);
    }

    private void downloadFromDatabase(){

        layoutInflater = getLayoutInflater();
        dbHelper.getDatabaseReference().addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for(DataSnapshot data : snapshot.child("USER_DATA").child("INTERNSHIPS").getChildren()){
                    if (rule.contains("salary!=не оплачивается")){
                        if (data.child("salary").getValue(String.class).contains("не оплачивается")){
                            ArrayList<String> params = new ArrayList<>();
                            String jobTitle = data.child("jobTitle").getValue(String.class);
                            String salary = data.child("salary").getValue(String.class);
                            String city = data.child("city").getValue(String.class);
                            String company = data.child("company").getValue(String.class);
                            String description = data.child("description").getValue(String.class);

                            CardView cardView = (CardView) layoutInflater.inflate(R.layout.internship_card, linearLayout, false);
                            RelativeLayout relativeLayout = (RelativeLayout) cardView.getChildAt(0);

                            TextView jobTitleView = (TextView) relativeLayout.getChildAt(0);
                            jobTitleView.setText(jobTitle);
                            TextView salaryView = (TextView) relativeLayout.getChildAt(1);
                            salaryView.setText(salary);
                            TextView adressView = (TextView) relativeLayout.getChildAt(2);
                            adressView.setText(city);
                            TextView companyView = (TextView) relativeLayout.getChildAt(3);
                            companyView.setText(company);
                            TextView descView = (TextView) relativeLayout.getChildAt(5);
                            descView.setText(description);
                            addToLinearLayout(cardView);
                        }
                    } else {
                        
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    public void init(){
        linearLayout = findViewById(R.id.dashboardSearchInternship);
        downloadFromDatabase();
    }
}