package com.example.edtechhackathon.Location;

import android.location.Location;

public interface LocationListenerInterface {

    public void OnLocationChanged(Location location);

}

