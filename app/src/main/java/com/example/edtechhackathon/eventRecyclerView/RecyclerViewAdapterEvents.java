package com.example.edtechhackathon.eventRecyclerView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.edtechhackathon.R;

import java.util.ArrayList;

public class RecyclerViewAdapterEvents extends RecyclerView.Adapter<RecyclerViewAdapterEvents.RecyclerViewViewHolder> {

    private ArrayList<RecyclerViewItemEvent> arrayList;
    protected Context context;

    @NonNull
    @Override
    public RecyclerViewViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.event_card,parent,false);
        RecyclerViewViewHolder recyclerViewViewHolder = new RecyclerViewViewHolder(view);
        return recyclerViewViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewViewHolder holder, int position) {
        RecyclerViewItemEvent recyclerViewItemEvent = arrayList.get(position);

        holder.textViewTitle.setText(recyclerViewItemEvent.getTitle());
        holder.textViewDesc.setText(recyclerViewItemEvent.getDescription());
        holder.textViewCity.setText(recyclerViewItemEvent.getCity());
        holder.textViewDate.setText(recyclerViewItemEvent.getDate());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, Integer.toString(position), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public RecyclerViewAdapterEvents(ArrayList<RecyclerViewItemEvent> arrayList, Context context){
        this.arrayList = arrayList;
        this.context = context;
    }

    public class RecyclerViewViewHolder extends RecyclerView.ViewHolder{

        public TextView textViewTitle;
        public TextView textViewDesc;
        public TextView textViewCity;
        public TextView textViewDate;

        public RecyclerViewViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewTitle=itemView.findViewById(R.id.header);
            textViewDesc=itemView.findViewById(R.id.subHeader);
            textViewCity=itemView.findViewById(R.id.eventCity);
            textViewDate=itemView.findViewById(R.id.eventDate);
        }
    }

}
