package com.example.edtechhackathon;

import android.content.Context;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class DBHelper {

    FirebaseAuth mAuth = FirebaseAuth.getInstance();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference();

    public DatabaseReference getDatabaseReference(){
        return myRef;
    }

    public void writeToDataBase(Context context, String child, String value){
        myRef.child(child).setValue(value).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
            }
        });
    }

    public void writeToDataBase(Context context, String child, String child1, String value){
        myRef.child(child).child(child1).setValue(value).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
            }
        });
    }

    public void writeToDataBase(Context context, String child, String child1, String child2, String value){
        myRef.child(child).child(child1).child(child2).setValue(value).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
            }
        });
    }
    public void writeToDataBase(Context context, String child, String child1, String child2, String child3, String value){
        myRef.child(child).child(child1).child(child2).child(child3).setValue(value).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
            }
        });
    }

    public void writeToDataBase(Context context, String child, String child1, String child2, String child3, String child4,String value){
        myRef.child(child).child(child1).child(child2).child(child3).child(child4).setValue(value).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
            }
        });
    }

    public FirebaseAuth getmAuth(){
        return mAuth;
    }
}
