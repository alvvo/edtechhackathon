package com.example.edtechhackathon.activities;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import com.example.edtechhackathon.DBHelper;
import com.example.edtechhackathon.FinalSearchActivity;
import com.example.edtechhackathon.R;
import com.example.edtechhackathon.fragments.DashboardStudentFragment;
import com.example.edtechhackathon.fragments.ProfileStudentFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.Locale;

public class BottomNavActivity extends AppCompatActivity {

    String role;

    BottomNavigationView bottomNavigationView;
    NavController navController;

    //Voice assistant
    private RelativeLayout parentRelativeLayout;
    private SpeechRecognizer speechRecognizerInMainActivity;
    private Intent speechRecognizerIntentInMainActivity;
    private AudioManager audioManager;
    private String keeper = "Вася";
    public static String nameOfAssistant = "Вася";
    public Dialog dialog;
    DashboardStudentFragment dashboardStudentFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bottom_nav);
        acceptIntent();
        bottomNavigationView = findViewById(R.id.bottom_nav); //удалить после фикса
        navController = Navigation.findNavController(this, R.id.fragment); //удалить после фикса
        init();

        NavigationUI.setupWithNavController(bottomNavigationView, navController);
    }

    private void init(){
        checkVoiceCommandPermission();

        //Mute sounds recognition
        audioManager=(AudioManager)getSystemService(Context.AUDIO_SERVICE);
        audioManager.setStreamMute(AudioManager.STREAM_NOTIFICATION, true);
        audioManager.setStreamMute(AudioManager.STREAM_ALARM, true);
        audioManager.setStreamMute(AudioManager.STREAM_MUSIC, true);
        audioManager.setStreamMute(AudioManager.STREAM_RING, true);
        audioManager.setStreamMute(AudioManager.STREAM_SYSTEM, true);

        speechRecognizerInMainActivity = SpeechRecognizer.createSpeechRecognizer(BottomNavActivity.this);
        speechRecognizerIntentInMainActivity = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        speechRecognizerIntentInMainActivity.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        speechRecognizerIntentInMainActivity.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        speechRecognizerInMainActivity.startListening(speechRecognizerIntentInMainActivity);
        setRecognitionListener();

        dashboardStudentFragment = new DashboardStudentFragment();
    }

    private void acceptIntent(){
        Intent intent = getIntent();
        if(intent.getStringExtra("role")!=null&&!(intent.getStringExtra("role").equals(""))){
            role = intent.getStringExtra("role");
        }
    }

    private void checkVoiceCommandPermission(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if (!(ContextCompat.checkSelfPermission(BottomNavActivity.this, Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED)){
                requestPermissions(new String[]{Manifest.permission.RECORD_AUDIO}, 99);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 99 && grantResults[0] == RESULT_OK){
            checkVoiceCommandPermission();
        }
    }

    public void setRecognitionListener() {
        speechRecognizerInMainActivity.setRecognitionListener(new RecognitionListener() {
            @Override
            public void onReadyForSpeech(Bundle bundle) {

            }

            @Override
            public void onBeginningOfSpeech() {

            }

            @Override
            public void onRmsChanged(float v) {

            }

            @Override
            public void onBufferReceived(byte[] bytes) {

            }

            @Override
            public void onEndOfSpeech() {
                speechRecognizerInMainActivity.startListening(speechRecognizerIntentInMainActivity);
                Log.i("!!!", "result");
            }

            @Override
            public void onError(int i) {
                speechRecognizerInMainActivity.startListening(speechRecognizerIntentInMainActivity);
            }

            @Override
            public void onResults(Bundle bundle) {
                ArrayList<String> matchesFound = bundle.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
                if (matchesFound != null){
                    String result = matchesFound.get(0);
                    phraseRecognition(result);
                    Log.i("!!!", result);
                }
            }

            @Override
            public void onPartialResults(Bundle bundle) {

            }

            @Override
            public void onEvent(int i, Bundle bundle) {

            }
        });
    }

    private void phraseRecognition(String text) {

        if (text.toLowerCase().contains(keeper.toLowerCase())){
            speechRecognizerInMainActivity.stopListening();
            speechRecognizerInMainActivity.destroy();
            Intent intent = new Intent(this, VoiceAssistantActivity.class);
            intent.putExtra("LastMessage", text);
            startActivity(intent);
        }

    }

    public void onClickSearchInternship(View view) {
        CardView cardView = (CardView) view;
        TextView rule = (TextView) cardView.getChildAt(0);
        Intent intent = new Intent(this, FinalSearchActivity.class);
        intent.putExtra("rule",rule.getText());
        startActivity(intent);
    }

    public void onEditSkillsClick(View view) {
        dialog = new Dialog(this);
        dialog.setContentView(R.layout.skill_card_add);
        dialog.getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.transparent));
        dialog.show();
        dialog.findViewById(R.id.addSkill).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DBHelper dbHelper = new DBHelper();
                Spinner vectorLearning=dialog.findViewById(R.id.vectorLearning);
                Spinner skills=dialog.findViewById(R.id.skillsSpinner);
                String skill = skills.getSelectedItem().toString();
                dbHelper.writeToDataBase(getApplicationContext(),"USER_DATA","STUDENTS",dbHelper.getmAuth().getUid(),"Skills",skill,skill);
                dialog.cancel();
            }
        });
        /*dialog.findViewById(R.id.createNoteButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText noteNameEditText = noteCreateDialog.findViewById(R.id.noteName);
                EditText noteDescriptionEditText = noteCreateDialog.findViewById(R.id.noteDescription);
                ArrayChanger arrayChanger = new ArrayChanger();
                arrayChanger.addElement(noteNameEditText.getText().toString(), noteDescriptionEditText.getText().toString());
                adapter.notifyDataSetChanged();
                noteCreateDialog.cancel();
            }
        });*/
        ProfileStudentFragment profileStudentFragment = new ProfileStudentFragment();

    }

    public void onClickGoToMap(View view) {
        Intent intent = new Intent(this, MapActivity.class);
        startActivity(intent);
    }

    public void onPersonInfoEditClick(View view) {
        dialog = new Dialog(this);
        dialog.setContentView(R.layout.edit_person_info_card);
        dialog.getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.transparent));
        dialog.show();
        dialog.findViewById(R.id.editPersonInfo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DBHelper dbHelper = new DBHelper();
                EditText name = dialog.findViewById(R.id.nameEditText);
                EditText city = dialog.findViewById(R.id.cityEditText);
                dbHelper.writeToDataBase(getApplicationContext(),"USER_DATA","STUDENTS",dbHelper.getmAuth().getUid(),"Name",name.getText().toString());
                dbHelper.writeToDataBase(getApplicationContext(),"USER_DATA","STUDENTS",dbHelper.getmAuth().getUid(),"city",city.getText().toString());
                dialog.cancel();
            }
        });
    }

    public void onEditUniversityInfoClick(View view) {
        dialog = new Dialog(this);
        dialog.setContentView(R.layout.edit_person_education_info_card);
        dialog.getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.transparent));
        dialog.show();
        dialog.findViewById(R.id.editPersonInfo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DBHelper dbHelper = new DBHelper();
                EditText university = dialog.findViewById(R.id.universityEditText);
                Spinner vectorLearning = dialog.findViewById(R.id.vectorLearning);
                dbHelper.writeToDataBase(getApplicationContext(),"USER_DATA","STUDENTS",dbHelper.getmAuth().getUid(),"university",university.getText().toString());
                dbHelper.writeToDataBase(getApplicationContext(),"USER_DATA","STUDENTS",dbHelper.getmAuth().getUid(),"specialization",vectorLearning.getSelectedItem().toString());
                dialog.cancel();
            }
        });
    }

    public void onCreateInternshipClick(View view) {
        Intent intent = new Intent(this, CreateInternshipActivity.class);
        startActivity(intent);
    }

    public void onTrackingClick(View view) {
        Intent intent = new Intent(this, TrackingInternshipActivity.class);
        startActivity(intent);
    }
}
