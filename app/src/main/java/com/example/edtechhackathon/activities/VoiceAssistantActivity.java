package com.example.edtechhackathon.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;

import com.airbnb.lottie.LottieAnimationView;
import com.example.edtechhackathon.AI;
import com.example.edtechhackathon.R;
import com.yandex.mapkit.MapKitFactory;

import java.util.ArrayList;
import java.util.Locale;

public class VoiceAssistantActivity extends AppCompatActivity {
    LayoutInflater layoutInflater;
    LinearLayout linearLayout;
    ScrollView scrollView;

    LottieAnimationView mic;

    //AI
    private AI ai;

    //Voice assistant
    private SpeechRecognizer speechRecognizer;
    private Intent speechRecognizerIntent;
    private AudioManager audioManager;
    private String keeper = "Алиса";

    private int state = 0;
    private String curPhrase;

    private TextToSpeech textToSpeech;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voice_assistant2);
        init();

    }

    private void init(){

        //Unmute sounds recognition
        audioManager=(AudioManager)getSystemService(Context.AUDIO_SERVICE);
        audioManager.setStreamMute(AudioManager.STREAM_NOTIFICATION, false);
        audioManager.setStreamMute(AudioManager.STREAM_ALARM, false);
        audioManager.setStreamMute(AudioManager.STREAM_MUSIC, false);
        audioManager.setStreamMute(AudioManager.STREAM_RING, false);
        audioManager.setStreamMute(AudioManager.STREAM_SYSTEM, false);

        linearLayout = findViewById(R.id.voiceAssistantScrollView);
        layoutInflater = getLayoutInflater();
        TextView messageRight = (TextView) layoutInflater.inflate(R.layout.message_right, linearLayout, false);

        Intent intent = getIntent();
        String lastMessage = intent.getStringExtra("LastMessage");

        speechRecognizer = SpeechRecognizer.createSpeechRecognizer(VoiceAssistantActivity.this);
        speechRecognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        speechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        speechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        setRecognitionListener();

        textToSpeech = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int i) {
                if (i == TextToSpeech.SUCCESS){
                    int result = textToSpeech.setLanguage(Locale.getDefault());
                    if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED){
                        Log.i("Error", "Language not supported!");
                    } else{
                    }
                }
            }
        });
        ai = new AI();

        if (lastMessage != null) {
            voiceAssistantController(lastMessage);
        }

        mic=findViewById(R.id.mic);
        scrollView=findViewById(R.id.scrollViewChat);
    }

    private void checkVoiceCommandPermission(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if (!(ContextCompat.checkSelfPermission(VoiceAssistantActivity.this, Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED)){
                requestPermissions(new String[]{Manifest.permission.RECORD_AUDIO}, 99);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 99 && grantResults[0] == RESULT_OK){
            checkVoiceCommandPermission();
        }
    }

    public void setRecognitionListener() {
        speechRecognizer.setRecognitionListener(new RecognitionListener() {
            @Override
            public void onReadyForSpeech(Bundle bundle) {

            }

            @Override
            public void onBeginningOfSpeech() {
                state = 1;
                startAnimation();
            }

            @Override
            public void onRmsChanged(float v) {

            }

            @Override
            public void onBufferReceived(byte[] bytes) {

            }

            @Override
            public void onEndOfSpeech() {
                state = 0;
                stopAnimation();
            }

            @Override
            public void onError(int i) {
                state = 0;
                stopAnimation();
            }

            @Override
            public void onResults(Bundle bundle) {
                ArrayList<String> matchesFound = bundle.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
                if (matchesFound != null){
                    String result = matchesFound.get(0);
                    voiceAssistantController(result);
                }
            }

            @Override
            public void onPartialResults(Bundle bundle) {

            }

            @Override
            public void onEvent(int i, Bundle bundle) {

            }
        });
    }

    private void addToLinearLayout(TextView view){
        linearLayout.addView(view);
        linearLayout.setBackgroundColor(getResources().getColor(R.color.transparent));
        linearLayout.invalidate();
        scrollView = findViewById(R.id.scrollViewChat);
        scrollView.post(new Runnable() {
            @Override
            public void run() {
                scrollView.fullScroll(ScrollView.FOCUS_DOWN);
            }
        });
    }

    private void addToLinearLayout(CardView view){
        linearLayout.addView(view);
        linearLayout.setBackgroundColor(getResources().getColor(R.color.transparent));
        linearLayout.invalidate();
        scrollView = findViewById(R.id.scrollViewChat);
        scrollView.post(new Runnable() {
            @Override
            public void run() {
                scrollView.fullScroll(ScrollView.FOCUS_DOWN);
            }
        });
    }

    private void voiceAssistantController(String text) {

        TextView messageRight = (TextView) layoutInflater.inflate(R.layout.message_right, linearLayout, false);
        TextView messageLeft = (TextView) layoutInflater.inflate(R.layout.message_left, linearLayout, false);
        CardView cardCanIHelp = (CardView) layoutInflater.inflate(R.layout.can_i_help_card, linearLayout, false);
        String message = refactorMessage(text);
        messageRight.setText(message);
        addToLinearLayout(messageRight);
        curPhrase = ai.recognizePhrase(text);
        if (curPhrase.compareTo("Давайте посмотрим") == 0){
            messageLeft.setText(curPhrase);
            addToLinearLayout(messageLeft);
            goToMap();
        } else if (curPhrase.compareTo("Что я умею?") == 0){
            messageLeft.setText(curPhrase);
            addToLinearLayout(messageLeft);
            addToLinearLayout(cardCanIHelp);
            speak(curPhrase);
            speechRecognizer.startListening(speechRecognizerIntent);
        } else if (text.contains(BottomNavActivity.nameOfAssistant)){
            curPhrase = "Чем я могу помочь?";
            messageLeft.setText(curPhrase);
            addToLinearLayout(messageLeft);
            speak(curPhrase);
            speechRecognizer.startListening(speechRecognizerIntent);
        } else {
            messageLeft.setText(curPhrase);
            addToLinearLayout(messageLeft);
            speak(curPhrase);
            speechRecognizer.startListening(speechRecognizerIntent);
        }
        Log.i("!@#", curPhrase);
    }

    private void goToMap(){
        Intent intent = new Intent(this, MapActivity.class);
        final String MAPKIT_API_KEY = "74454f0a-6b30-462c-a0e9-69fa27fd19d5";
        MapKitFactory.setApiKey(MAPKIT_API_KEY);
        startActivity(intent);
    }

    public void onClickChangeState(View view) {
        if (state == 0){
            speechRecognizer.startListening(speechRecognizerIntent);
        } else{
            state = 0;
            speechRecognizer.stopListening();
        }
    }

    public void startAnimation(){
        mic.setSpeed(2);
        mic.loop(true);
        mic.playAnimation();
    }

    public void stopAnimation(){
        mic.setFrame(0);
        mic.cancelAnimation();
    }

    public void onClickCallWhatCanYouDo(View view) {
        voiceAssistantController("Что ты умеешь?");
    }

    public void onClickGoToMap(View view) {
        voiceAssistantController("Покажи стажировки на карте?");
    }

    public String refactorMessage(String msg){
        if (msg.toLowerCase().contains("как") || msg.toLowerCase().contains("что") || msg.toLowerCase().contains("какой")){
            return msg + "?";
        }
        return msg;
    }

    private void speak(String text){

        new Thread(new Runnable() {
            public void run() {
                textToSpeech.setSpeechRate(1.3f);
                int speech = textToSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, null);
            }
        }).start();

    }

    @Override
    protected void onDestroy() {
        if (textToSpeech != null){
            textToSpeech.stop();
            textToSpeech.shutdown();
        }
        super.onDestroy();
    }
}