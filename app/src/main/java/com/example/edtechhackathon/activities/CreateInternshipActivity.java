package com.example.edtechhackathon.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.edtechhackathon.DBHelper;
import com.example.edtechhackathon.R;

import java.text.DateFormat;
import java.util.Date;

public class CreateInternshipActivity extends AppCompatActivity {

    EditText jobTitleEditText;
    EditText salaryEditText;
    EditText companyEditText;
    EditText descriptionEditText;
    EditText cityEditText;
    EditText expEditText;
    EditText reqEditText;
    EditText respEditText;
    EditText condEditText;
    EditText workTimeEditText;

    Button createInternshipButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_internship);
        init();

    }

    private void init(){
        jobTitleEditText=findViewById(R.id.job_input);
        salaryEditText=findViewById(R.id.salary_input);
        companyEditText=findViewById(R.id.company_input);
        descriptionEditText=findViewById(R.id.description_input);
        cityEditText=findViewById(R.id.city_input);
        expEditText=findViewById(R.id.exp_input);
        reqEditText=findViewById(R.id.req_input);
        respEditText=findViewById(R.id.resp_input);
        condEditText=findViewById(R.id.cond_input);
        workTimeEditText=findViewById(R.id.workTime_input);

        createInternshipButton=findViewById(R.id.accept);
    }

    public void onCreateButtonClick(View view) {
        DBHelper dbHelper = new DBHelper();

        dbHelper.writeToDataBase(this,"USER_DATA","INTERNSHIPS",dbHelper.getmAuth().getUid(),"jobTitle",jobTitleEditText.getText().toString());
        dbHelper.writeToDataBase(this,"USER_DATA","INTERNSHIPS",dbHelper.getmAuth().getUid(),"salary",salaryEditText.getText().toString());
        dbHelper.writeToDataBase(this,"USER_DATA","INTERNSHIPS",dbHelper.getmAuth().getUid(),"company",cityEditText.getText().toString());
        dbHelper.writeToDataBase(this,"USER_DATA","INTERNSHIPS",dbHelper.getmAuth().getUid(),"description",descriptionEditText.getText().toString());
        dbHelper.writeToDataBase(this,"USER_DATA","INTERNSHIPS",dbHelper.getmAuth().getUid(),"city",cityEditText.getText().toString());
        dbHelper.writeToDataBase(this,"USER_DATA","INTERNSHIPS",dbHelper.getmAuth().getUid(),"exp",expEditText.getText().toString());
        dbHelper.writeToDataBase(this,"USER_DATA","INTERNSHIPS",dbHelper.getmAuth().getUid(),"requirements",reqEditText.getText().toString());
        dbHelper.writeToDataBase(this,"USER_DATA","INTERNSHIPS",dbHelper.getmAuth().getUid(),"responsibilities",respEditText.getText().toString());
        dbHelper.writeToDataBase(this,"USER_DATA","INTERNSHIPS",dbHelper.getmAuth().getUid(),"conditions",condEditText.getText().toString());
        dbHelper.writeToDataBase(this,"USER_DATA","INTERNSHIPS",dbHelper.getmAuth().getUid(),"workTime",workTimeEditText.getText().toString());
        Date date = new Date();
        String stringDate = DateFormat.getDateTimeInstance().format(date);
        dbHelper.writeToDataBase(this,"USER_DATA","INTERNSHIPS",dbHelper.getmAuth().getUid(),"date",stringDate);
        dbHelper.writeToDataBase(this,"USER_DATA","INTERNSHIPS",dbHelper.getmAuth().getUid(),"UID",dbHelper.getmAuth().getUid());

        Toast.makeText(this, "Вакансия создана", Toast.LENGTH_SHORT).show();

        goToMenu();
    }

    private void goToMenu(){
        Intent intent = new Intent(this,BottomNavActivity.class);
        startActivity(intent);
    }
}