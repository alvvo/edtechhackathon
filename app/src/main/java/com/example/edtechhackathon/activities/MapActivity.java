package com.example.edtechhackathon.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;

import com.example.edtechhackathon.DBHelper;
import com.example.edtechhackathon.Location.LocationListenerInterface;
import com.example.edtechhackathon.Location.MyLocationListener;
import com.example.edtechhackathon.R;
import com.example.edtechhackathon.dashboardRecyclerView.RecyclerViewItem;
import com.example.edtechhackathon.entities.Coords;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.yandex.mapkit.Animation;
import com.yandex.mapkit.MapKitFactory;
import com.yandex.mapkit.geometry.Point;
import com.yandex.mapkit.map.CameraPosition;
import com.yandex.mapkit.map.MapObject;
import com.yandex.mapkit.map.MapObjectCollection;
import com.yandex.mapkit.map.MapObjectTapListener;
import com.yandex.mapkit.map.PlacemarkMapObject;
import com.yandex.mapkit.mapview.MapView;
import com.yandex.runtime.ui_view.ViewProvider;

import java.util.ArrayList;
import java.util.List;

public class MapActivity extends AppCompatActivity implements LocationListenerInterface {

    //Points
    private final Point DEFAULT_CAMERA_TARGET = new Point(55.738094, 37.620456);

    private MapView mapView;
    private MapObjectCollection mapObjects;
    private Handler animationHandler;

    //Location
    private LocationManager locationManager;
    private Location lastLocation;
    private MyLocationListener locationListener;

    //Coords
    private List<Coords> coordsList;
    ArrayList<RecyclerViewItem> recyclerViewItems = new ArrayList<RecyclerViewItem>();
    DBHelper dbHelper = new DBHelper();

    private CardView shortCard;
    private boolean state = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        init();
        super.onCreate(savedInstanceState);
        MapKitFactory.initialize(MapActivity.this);
        setContentView(R.layout.activity_map);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        mapView = (MapView) findViewById(R.id.mapview);
        if (location != null){
            mapView.getMap().move(
                    new CameraPosition(new Point(location.getLatitude(), location.getLongitude()), 15.0f, 0.0f, 0.0f),
                    new Animation(Animation.Type.SMOOTH, 0), null);
        } else{
            mapView.getMap().move(
                    new CameraPosition(DEFAULT_CAMERA_TARGET, 15.0f, 0.0f, 0.0f),
                    new Animation(Animation.Type.SMOOTH, 0), null);
        }
        mapObjects = mapView.getMap().getMapObjects().addCollection();
        animationHandler = new Handler();
        Log.i("!@#", String.valueOf(lastLocation));

    }

    private void init() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        locationListener = new MyLocationListener();
        locationListener.setLocationListenerInterface(this);
        checkPermissions();
        downloadItemsFromDatabase();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 100 && grantResults[0] == RESULT_OK) {
            checkPermissions();
        }
    }

    private void checkPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 100);
        } else {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 1, locationListener);
        }
    }

    @Override
    public void OnLocationChanged(Location location) {
        lastLocation = location;
    }

    @Override
    protected void onStop() {
        mapView.onStop();
        MapKitFactory.getInstance().onStop();
        super.onStop();
    }

    @Override
    protected void onStart() {
        super.onStart();
        MapKitFactory.getInstance().onStart();
        mapView.onStart();
    }

    public void toCurLocation(View view) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        Log.i("!@#", String.valueOf(location));
        if (location != null){
            mapView.getMap().move(
                    new CameraPosition(new Point(location.getLatitude(), location.getLongitude()), 15.0f, 0.0f, 0.0f),
                    new Animation(Animation.Type.SMOOTH, 0), null);
        } else {
            Toast.makeText(this, "Включите геолокацию", Toast.LENGTH_SHORT).show();
        }
    }

    public void zoom(View view) {
        float zoom = mapView.getMap().getCameraPosition().getZoom();
        float maxZoom = mapView.getMap().getMaxZoom();
        double latitude = mapView.getMap().getCameraPosition().getTarget().getLatitude();
        double longitude = mapView.getMap().getCameraPosition().getTarget().getLongitude();
        if (zoom <= maxZoom){
            mapView.getMap().move(
                    new CameraPosition(new Point(latitude, longitude), zoom + 1f, 0.0f, 0.0f),
                    new Animation(Animation.Type.SMOOTH, 0), null);
        }
    }

    public void unzoom(View view) {
        float zoom = mapView.getMap().getCameraPosition().getZoom();
        float minZoom = mapView.getMap().getMinZoom();
        double latitude = mapView.getMap().getCameraPosition().getTarget().getLatitude();
        double longitude = mapView.getMap().getCameraPosition().getTarget().getLongitude();
        if (zoom >= minZoom){
            mapView.getMap().move(
                    new CameraPosition(new Point(latitude, longitude), zoom - 1f, 0.0f, 0.0f),
                    new Animation(Animation.Type.SMOOTH, 0), null);
        }
    }

    public void go_to_List(View view) {
        Intent intent = new Intent(MapActivity.this, BottomNavActivity.class);
        startActivity(intent);
    }

    @SuppressLint("ResourceType")
    private void createPlacemarkMapObjectWithViewProvider(Point coords, String id) {

        ImageView imageView = new ImageView(getApplicationContext());
        imageView.setImageResource(R.drawable.ic_placeholder);

        final ViewProvider viewProvider = new ViewProvider(imageView);
        final PlacemarkMapObject viewPlacemark = mapObjects.addPlacemark(coords, viewProvider);
        viewPlacemark.setUserData(new Id(id));
        viewPlacemark.addTapListener(mapObjectTapListener);
    }

    private void downloadItemsFromDatabase(){
        dbHelper.getDatabaseReference().child("USER_DATA").child("INTERNSHIPS").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for(DataSnapshot data : snapshot.getChildren()){
                    String city = data.child("city").getValue(String.class);
                    Log.i("!@#", city);
                    Coords coords = new Coords();
                    coords.createCoords(city);
                    createPlacemarkMapObjectWithViewProvider(new Point(coords.getLatitude(), coords.getLongitude()), data.child("UID").getValue(String.class));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private MapObjectTapListener mapObjectTapListener = new MapObjectTapListener() {
        @Override
        public boolean onMapObjectTap(@NonNull MapObject mapObject, @NonNull Point point) {
            CardView cardView = findViewById(R.id.short_card);
            Id id = (Id) mapObject.getUserData();
            cardView = readShortCardFromDatabase(cardView, id);
            cardView.setVisibility(View.VISIBLE);
            return true;
        }
    };

    private CardView readShortCardFromDatabase(CardView view, Id id){

        RelativeLayout layout = (RelativeLayout) view.getChildAt(0);
        TextView textView = findViewById(R.id.uid);
        dbHelper.getDatabaseReference().child("USER_DATA").child("INTERNSHIPS").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for(DataSnapshot data : snapshot.getChildren()){
                    String uid = data.child("UID").getValue(String.class);
                    ArrayList<String> params = new ArrayList<>();
                    if (uid.contains(id.getId())){
                        params.add(data.child("jobTitle").getValue(String.class));
                        params.add(data.child("salary").getValue(String.class));
                        params.add(data.child("city").getValue(String.class));
                        params.add(data.child("company").getValue(String.class));
                        params.add(data.child("description").getValue(String.class));
                        params.add(uid);
                        RelativeLayout layout = (RelativeLayout) view.getChildAt(0);
                        for (int i = 0; i < layout.getChildCount() - 1; i++){
                            TextView textView = (TextView) layout.getChildAt(i);
                            textView.setText(params.get(i));
                        }
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        return view;
    }

    public void onCloseShortInternshipCardClick(View view) {
        CardView cardView = (CardView) view.getParent().getParent();
        cardView.setVisibility(View.INVISIBLE);
    }

    public void onClickGoToInternshipActivity(View view) {
        TextView textView = findViewById(R.id.uid);
        Intent intent = new Intent(this, InternshipViewActivity.class);
        intent.putExtra("UID", textView.getText().toString());
        startActivity(intent);
    }
}

