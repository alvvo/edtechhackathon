package com.example.edtechhackathon.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.edtechhackathon.DBHelper;
import com.example.edtechhackathon.R;
import com.example.edtechhackathon.dashboardRecyclerView.RecyclerViewItem;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

public class InternshipViewActivity extends AppCompatActivity {

    DBHelper dbHelper = new DBHelper();
    String UID;
    String role = "";
    String descriptionString;

    TextView jobTitle;
    TextView salary;
    TextView company;
    TextView city;
    TextView description;
    TextView exp;
    TextView responsibilities;
    TextView requirements;
    TextView conditions;
    TextView date;

    Button buttonOtclick;
    Button deleteButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_internship_view);
        init();
        buttonOtclick.setVisibility(View.GONE);
        deleteButton.setVisibility(View.GONE);

        dbHelper.getDatabaseReference().child("USER_DATA").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for(DataSnapshot data : snapshot.child("UNIVERSITIES").getChildren()){
                    if(dbHelper.getmAuth().getUid().equals(data.getKey())){
                        buttonOtclick.setVisibility(View.GONE);
                        deleteButton.setVisibility(View.GONE);
                        role = data.child("Role").getValue(String.class);
                        saveRole(role);
                        dbHelper.getDatabaseReference().child("USER_DATA").child("INTERNSHIPS").addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot snapshot) {
                                for(DataSnapshot dataSnapshot : snapshot.getChildren()){
                                    if(dataSnapshot.child("UID").getValue(String.class).equals(UID)){
                                        RecyclerViewItem recyclerViewItem = dataSnapshot.getValue(RecyclerViewItem.class);
                                        jobTitle.setText(recyclerViewItem.getJobTitle());
                                        salary.setText(recyclerViewItem.getSalary());
                                        company.setText(recyclerViewItem.getCompany());
                                        city.setText(recyclerViewItem.getCity());
                                        description.setText(recyclerViewItem.getDescription());
                                        exp.setText(recyclerViewItem.getExp());
                                        responsibilities.setText(recyclerViewItem.getResponsibilities());
                                        requirements.setText(recyclerViewItem.getRequirements());
                                        conditions.setText(recyclerViewItem.getConditions());
                                        date.setText(recyclerViewItem.getDate());
                                    }
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError error) {

                            }
                        });
                    }
                }
                for(DataSnapshot data : snapshot.child("STUDENTS").getChildren()){
                    if(dbHelper.getmAuth().getUid().equals(data.getKey())){
                        role = data.child("Role").getValue(String.class);
                        buttonOtclick.setVisibility(View.VISIBLE);
                        saveRole(role);
                        deleteButton.setVisibility(View.GONE);
                        dbHelper.getDatabaseReference().child("USER_DATA").child("INTERNSHIPS").addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot snapshot) {
                                for(DataSnapshot dataSnapshot : snapshot.getChildren()){
                                    if(dataSnapshot.child("UID").getValue(String.class).equals(UID)){
                                        RecyclerViewItem recyclerViewItem = dataSnapshot.getValue(RecyclerViewItem.class);
                                        jobTitle.setText(recyclerViewItem.getJobTitle());
                                        salary.setText(recyclerViewItem.getSalary());
                                        company.setText(recyclerViewItem.getCompany());
                                        city.setText(recyclerViewItem.getCity());
                                        description.setText(recyclerViewItem.getDescription());
                                        exp.setText(recyclerViewItem.getExp());
                                        responsibilities.setText(recyclerViewItem.getResponsibilities());
                                        requirements.setText(recyclerViewItem.getRequirements());
                                        conditions.setText(recyclerViewItem.getConditions());
                                        date.setText(recyclerViewItem.getDate());
                                    }
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError error) {

                            }
                        });
                    }
                }
                for(DataSnapshot data : snapshot.child("ADMINS").getChildren()){
                    if(dbHelper.getmAuth().getUid().equals(data.getKey())){
                        buttonOtclick.setVisibility(View.GONE);
                        role = data.child("Role").getValue(String.class);
                        saveRole(role);
                        dbHelper.getDatabaseReference().child("USER_DATA").child("INTERNSHIPS").addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot snapshot) {
                                for(DataSnapshot dataSnapshot : snapshot.getChildren()){
                                    if(dataSnapshot.child("UID").getValue(String.class).equals(UID)){
                                        RecyclerViewItem recyclerViewItem = dataSnapshot.getValue(RecyclerViewItem.class);
                                        jobTitle.setText(recyclerViewItem.getJobTitle());
                                        salary.setText(recyclerViewItem.getSalary());
                                        company.setText(recyclerViewItem.getCompany());
                                        city.setText(recyclerViewItem.getCity());
                                        description.setText(recyclerViewItem.getDescription());
                                        exp.setText(recyclerViewItem.getExp());
                                        responsibilities.setText(recyclerViewItem.getResponsibilities());
                                        requirements.setText(recyclerViewItem.getRequirements());
                                        conditions.setText(recyclerViewItem.getConditions());
                                        date.setText(recyclerViewItem.getDate());
                                    }
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError error) {

                            }
                        });
                    }
                }
                for(DataSnapshot data : snapshot.child("PARTNERS").getChildren()){
                    if(dbHelper.getmAuth().getUid().equals(data.getKey())){
                        buttonOtclick.setVisibility(View.GONE);
                        deleteButton.setVisibility(View.GONE);
                        role = data.child("Role").getValue(String.class);
                        saveRole(role);
                        dbHelper.getDatabaseReference().child("USER_DATA").child("INTERNSHIPS").addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot snapshot) {
                                for(DataSnapshot dataSnapshot : snapshot.getChildren()){
                                    if(dataSnapshot.child("UID").getValue(String.class).equals(UID)){
                                        RecyclerViewItem recyclerViewItem = dataSnapshot.getValue(RecyclerViewItem.class);
                                        jobTitle.setText(recyclerViewItem.getJobTitle());
                                        salary.setText(recyclerViewItem.getSalary());
                                        company.setText(recyclerViewItem.getCompany());
                                        city.setText(recyclerViewItem.getCity());
                                        description.setText(recyclerViewItem.getDescription());
                                        exp.setText(recyclerViewItem.getExp());
                                        responsibilities.setText(recyclerViewItem.getResponsibilities());
                                        requirements.setText(recyclerViewItem.getRequirements());
                                        conditions.setText(recyclerViewItem.getConditions());
                                        date.setText(recyclerViewItem.getDate());
                                    }
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError error) {

                            }
                        });

                        if(UID.equals(dbHelper.getmAuth().getUid())){
                            deleteButton.setVisibility(View.VISIBLE);
                        }
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }


    private void saveRole(String role){
        this.role=role;
    }

    private void init(){
        acceptIntent();

        jobTitle=findViewById(R.id.job_title);
        salary=findViewById(R.id.salary);
        company=findViewById(R.id.company);
        city=findViewById(R.id.adress);
        description=findViewById(R.id.description);
        exp=findViewById(R.id.exp);
        responsibilities=findViewById(R.id.responsibilities);
        requirements=findViewById(R.id.requirements);
        conditions=findViewById(R.id.conditions);
        date=findViewById(R.id.date);

        buttonOtclick=findViewById(R.id.otclick);
        deleteButton=findViewById(R.id.daleteButton);
    }

    private void acceptIntent(){
        Intent intent = getIntent();
        UID = intent.getStringExtra("UID");
        descriptionString = intent.getStringExtra("desc");
    }

    public void onClickAddResponse(View view) {
        Log.i("sss", view.toString());
        dbHelper.getDatabaseReference().child("USER_DATA").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot data : snapshot.child("INTERNSHIPS").getChildren()){
                    String uid = getIntent().getStringExtra("UID");
                    if (data.child("UID").getValue(String.class).contains(uid)){
                        Toast.makeText(InternshipViewActivity.this, "Вы успешно откликнулись на стажировку, ожидайте ответа", Toast.LENGTH_LONG).show();
                        dbHelper.writeToDataBase(InternshipViewActivity.this, "USER_DATA", "INTERNSHIPS", data.getKey(), "Responses", getIntent().getStringExtra("UID"), uid);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
}