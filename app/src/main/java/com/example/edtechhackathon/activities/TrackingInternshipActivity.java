package com.example.edtechhackathon.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.Adapter;
import android.widget.TextView;

import com.example.edtechhackathon.DBHelper;
import com.example.edtechhackathon.R;
import com.example.edtechhackathon.dashboardRecyclerView.RecyclerViewAdapter;
import com.example.edtechhackathon.dashboardRecyclerView.RecyclerViewItem;
import com.example.edtechhackathon.eventRecyclerView.RecyclerViewItemEvent;
import com.example.edtechhackathon.internshipsRequestRecyclerView.RecyclerViewAdapterRequests;
import com.example.edtechhackathon.internshipsRequestRecyclerView.RecyclerViewItemRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class TrackingInternshipActivity extends AppCompatActivity {

    ArrayList<RecyclerViewItemRequest> arrayList = new ArrayList<>();

    RecyclerView recyclerView;

    DBHelper dbHelper = new DBHelper();

    RecyclerViewAdapterRequests adapter;
    RecyclerView.LayoutManager layoutManager;

    TextView countTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tracking_internship);
        init();

        dbHelper.getDatabaseReference().addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for(DataSnapshot data : snapshot.child("USER_DATA").child("INTERNSHIPS").getChildren()){
                    if(data.child("UID").getValue().equals(dbHelper.getmAuth().getUid())){
                        for(DataSnapshot dataSnapshot : data.child("Responses").getChildren()){
                            String str = dataSnapshot.getValue(String.class);
                            RecyclerViewItemRequest recyclerViewItem = new RecyclerViewItemRequest(str);
                            dbHelper.getDatabaseReference().child("USER_DATA").child("STUDENTS").addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot snapshot) {
                                    for(DataSnapshot data:snapshot.getChildren()){
                                        if(recyclerViewItem.getName().equals(data.getKey())){
                                            recyclerViewItem.setName(data.child("Name").getValue().toString());
                                            arrayList.add(recyclerViewItem);
                                            adapter.notifyDataSetChanged();
                                            countTextView.setText(Integer.toString(arrayList.size()));
                                        }
                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError error) {

                                }
                            });
                        }
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }

    private void init(){
        recyclerView=findViewById(R.id.studentRecycler);
        adapter= new RecyclerViewAdapterRequests(arrayList,getApplicationContext());
        layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(layoutManager);
        countTextView=findViewById(R.id.count);
    }
}