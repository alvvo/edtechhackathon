package com.example.edtechhackathon.dashboardRecyclerView;

import java.text.SimpleDateFormat;
import java.util.Date;

public class RecyclerViewItem {

    private String jobTitle;
    private String salary;
    private String company;
    private String city;
    private String description;
    private String date;
    private String UID;
    private String exp;
    private String workTime;
    private String responsibilities;
    private String requirements;
    private String conditions;

    private String ID;

    public RecyclerViewItem(String jobTitle, String salary, String company, String city, String description, String date, String UID, String exp, String workTime, String responsibilities, String requirements, String conditions, String ID) {
        this.jobTitle = jobTitle;
        this.salary = salary;
        this.company = company;
        this.city = city;
        this.description = description;
        this.date = date;
        this.UID = UID;
        this.exp = exp;
        this.workTime = workTime;
        this.responsibilities = responsibilities;
        this.requirements = requirements;
        this.conditions = conditions;
        this.ID = ID;
    }

    public RecyclerViewItem() {
        this.jobTitle = "";
        this.salary = "";
        this.company = "";
        this.city = "";
        this.description = "";
        this.date = "";
        this.ID = "";
        this.UID = "";
        this.exp = "";
        this.workTime = "";
        this.responsibilities = "";
        this.requirements = "";
        this.conditions = "";
    }

    public String getExp() {
        return exp;
    }

    public void setExp(String exp) {
        this.exp = exp;
    }

    public String getWorkTime() {
        return workTime;
    }

    public void setWorkTime(String workTime) {
        this.workTime = workTime;
    }

    public String getResponsibilities() {
        return responsibilities;
    }

    public void setResponsibilities(String responsibilities) {
        this.responsibilities = responsibilities;
    }

    public String getRequirements() {
        return requirements;
    }

    public void setRequirements(String requirements) {
        this.requirements = requirements;
    }

    public String getConditions() {
        return conditions;
    }

    public void setConditions(String conditions) {
        this.conditions = conditions;
    }

    public String getUID() {
        return UID;
    }

    public void setUID(String UID) {
        this.UID = UID;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
