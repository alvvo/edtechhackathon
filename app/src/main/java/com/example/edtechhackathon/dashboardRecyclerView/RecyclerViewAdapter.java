package com.example.edtechhackathon.dashboardRecyclerView;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.edtechhackathon.DBHelper;
import com.example.edtechhackathon.R;
import com.example.edtechhackathon.activities.InternshipViewActivity;

import java.util.ArrayList;

public class RecyclerViewAdapter extends RecyclerView.Adapter<com.example.edtechhackathon.dashboardRecyclerView.RecyclerViewAdapter.RecyclerViewViewHolder> {

    private ArrayList<com.example.edtechhackathon.dashboardRecyclerView.RecyclerViewItem> arrayList;
    protected Context context;

    @NonNull
    @Override
    public RecyclerViewViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.internship_card,parent,false);
        RecyclerViewViewHolder recyclerViewViewHolder = new RecyclerViewViewHolder(view);
        return recyclerViewViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull com.example.edtechhackathon.dashboardRecyclerView.RecyclerViewAdapter.RecyclerViewViewHolder holder, int position) {
        RecyclerViewItem recyclerViewItem  = arrayList.get(position);

        holder.textVieJobTitle.setText(recyclerViewItem.getJobTitle());
        holder.textViewSalary.setText(recyclerViewItem.getSalary());
        holder.textViewCompany.setText(recyclerViewItem.getCompany());
        holder.textViewCity.setText(recyclerViewItem.getCity());
        holder.textViewDescription.setText(recyclerViewItem.getDescription());
        holder.textViewDate.setText(recyclerViewItem.getDate());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,InternshipViewActivity.class);
                intent.putExtra("UID",recyclerViewItem.getUID());
                intent.putExtra("desc",recyclerViewItem.getDescription());
                ContextCompat.startActivity(context,intent,null);
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public RecyclerViewAdapter(ArrayList<com.example.edtechhackathon.dashboardRecyclerView.RecyclerViewItem> arrayList, Context context){
        this.arrayList = arrayList;
        this.context = context;
    }

    public class RecyclerViewViewHolder extends RecyclerView.ViewHolder{

        public TextView textVieJobTitle;
        public TextView textViewSalary;
        public TextView textViewCompany;
        public TextView textViewCity;
        public TextView textViewDescription;
        public TextView textViewDate;

        public RecyclerViewViewHolder(@NonNull View itemView) {
            super(itemView);
            textVieJobTitle=itemView.findViewById(R.id.job_title);
            textViewSalary=itemView.findViewById(R.id.salary);
            textViewCompany=itemView.findViewById(R.id.company);
            textViewCity=itemView.findViewById(R.id.adress);
            textViewDescription=itemView.findViewById(R.id.description);
            textViewDate=itemView.findViewById(R.id.date);
        }
    }

}
